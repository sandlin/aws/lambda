
###############################################################################
# Create the infrastructure for a lambda / flask / postgres app in AWS.
# -----------------------------------------------------------------------------
#
###############################################################################
provider "aws" {
  region = var.region
}
terraform {
  required_providers {
    aws = "~> 3.0"
  }
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/23807395/terraform/state/groot"
    lock_address = "https://gitlab.com/api/v4/projects/23807395/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/23807395/terraform/state/groot/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}

resource "random_pet" "this" {
  length = 2
}

locals {
  lambda_bucket_name = "lambda-${random_pet.this.id}"
}

data "aws_canonical_user_id" "current" {}

resource "aws_kms_key" "objects" {
  description             = "KMS key is used to encrypt bucket objects"
  deletion_window_in_days = 7
}


resource "aws_iam_role" "bucket_role" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    principals {
      type        = "AWS"
      identifiers = [aws_iam_role.bucket_role.arn]
    }

    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::${local.lambda_bucket_name}",
    ]
  }
}



resource "aws_s3_bucket" "lambda" {

  bucket = local.lambda_bucket_name
  acl    = "private"
  policy = data.aws_iam_policy_document.bucket_policy.json

  tags = {
    gl_owner_email_handle             = "jsandlin"
    gl_realm                          = "sales-cs"
    gl_dept                           = "sales-cs"
    gl_dept_group                     = "sales-cs-demo-cloud"
    gl_env_type                       = "dev"
    gl_env_name                       = "lambda"
    gl_sandbox_shutdown_after_days    = null
    gl_sandbox_shutdown_working_hours = true
    Terraform = "true"
  }

}


resource "aws_iam_user" "lambda_deployer" {
  name = "lambda_deployer"

  tags = {
    gl_owner_email_handle             = "jsandlin"
    gl_realm                          = "sales-cs"
    gl_dept                           = "sales-cs"
    gl_dept_group                     = "sales-cs-demo-cloud"
    gl_env_type                       = "dev"
    gl_env_name                       = "lambda_flask"
    gl_sandbox_shutdown_after_days    = null
    gl_sandbox_shutdown_working_hours = true
    Terraform = "true"
  }
}

resource "aws_iam_access_key" "lambda_deployer" {
  user = aws_iam_user.lambda_deployer.name
}

resource "aws_iam_user_policy" "lambda_deployer" {
  name = "lambda_deployer"
  user = aws_iam_user.lambda_deployer.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Sid": "",
        "Effect": "Allow",

      "Action": [
        "apigateway:*",
        "cloudformation:*",
        "dynamodb:*",
        "ec2:*",
        "events:*",
        "iam:*",
        "es:*",
        "kinesis:*",
        "lambda:*",
        "logs:*",
        "s3:*",
        "sns:*",
        "states:*"
      ],
        "Resource": "*"
    }
  ]
}
EOF

}
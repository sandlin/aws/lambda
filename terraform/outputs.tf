
output "bucket_id" {
  description = "The name of the bucket."
  value = aws_s3_bucket.lambda.id
}

output "bucket_domain_name" {
  description = "The bucket_domain_name of the bucket."
  value = aws_s3_bucket.lambda.bucket_domain_name
}

output "bucket_arn" {
  description = "The ARN of the bucket. Will be of format arn:aws:s3:::bucketname."
  value       = aws_s3_bucket.lambda.arn
}
output "lambda_deployer_name" {
  description = "The name of the iam_user"
  value       = aws_iam_user_policy.lambda_deployer.name
}


output "lambda_deployer_arn" {
  description = "The ARN of the iam_user. Will be of format arn:aws:s3:::bucketname."
  value       = aws_iam_user.lambda_deployer.arn
}



output "lambda_deployer_unique_id" {
  description = "The unique_id of the iam_user."
  value       = aws_iam_user_policy.lambda_deployer.id
}


output "lambda_deployer_user" {
  description = "The username of the deployer."
  value = aws_iam_access_key.lambda_deployer.user
}

output "AWS_ACCESS_KEY_ID" {
  description = "The access key ID of the deployer."
  value = aws_iam_access_key.lambda_deployer.id
}

output "AWS_ACCESS_KEY_" {
  description = "The secret access key of the deployer"
  value = aws_iam_access_key.lambda_deployer.secret
}